const TransactionController = artifacts.require("TransactionController");
const ERC20 = artifacts.require("BasicToken.sol")

const truffleAssert = require('truffle-assertions');

contract('TransactionController', (accounts) => {

    let rc20;
    let transactionController;
    let userA;
    let userB;
    let amount;

    before(async () => {
        rc20 = await ERC20.new();
        transactionController = await TransactionController.new(rc20.address);
        userA = accounts[0];
        userB = accounts[1];
        amount = web3.utils.toBN(1);
    })

    it('I can transfer to the contract', async () => {
      const retTransfer = await rc20.transfer(userB, amount, {from: userA});
      truffleAssert.eventEmitted(retTransfer, 'Transfer', (ev) => {
        return ev.from === userA && ev.to === userB && ev.value.toNumber() === amount.toNumber();
      }, 'Transfer should be emmited correctly');
      
    });
    it('The contract can send funds to other user', async () => {
        await rc20.transfer(transactionController.address, amount, {from: userA});
        await truffleAssert.passes(
            transactionController.doTransfer(userA, userB, amount), 
            "The doTransfer must finalize fine.");
        const size = await transactionController.getSize();
        assert.equal(size, 1);
        const transaction = await transactionController.getTransactions(0); 
        assert.equal(transaction._from, userA);
        assert.equal(transaction._to, userB);
        assert.equal(transaction._amount.toNumber(), amount.toNumber());
    });
    it('If I do not send funds to the contract, it fails', async () => {
        await truffleAssert.fails(
            transactionController.doTransfer(userA, userB, amount),
            truffleAssert.ErrorType.REVERT,
            "User without funds"
        );
    });
  });