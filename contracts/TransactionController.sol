pragma solidity ^0.5.0;

import "./ERC20Basic.sol";


contract TransactionController {

    struct Transaction {
        address from;
        address to;
        uint256 amount;
    }

    ERC20Basic erc20Basic;
    Transaction[] transactions;

    constructor (address rc20) public {
        erc20Basic = ERC20Basic(rc20);
    }

    function doTransfer(address _from, address _to, uint256 _amount) public {
        require(erc20Basic.balanceOf(address(this)) >= _amount, "User without funds");
        erc20Basic.transfer(_to, _amount);
        _addTransaction(_from, _to, _amount);
    }

    function getSize() public view returns(uint256 size) {
        size = transactions.length;
    }

    function getTransactions(uint256 pos) public view returns(address _from, address _to, uint256 _amount) {
        _from = transactions[pos].from;
        _to = transactions[pos].to;
        _amount = transactions[pos].amount;
    }

    function _addTransaction(address _from, address _to, uint256 _amount) private {
        Transaction memory transaction;
        transaction.from = _from;
        transaction.to = _to;
        transaction.amount = _amount;
        transactions.push(transaction);
    }

}